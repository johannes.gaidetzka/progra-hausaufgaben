public class zinseszinsRechner {
    public static void main(String[] args) {
        double startbetrag;
        double zinssatz;
        startbetrag = SimpleIO.getDouble("Bitte geben Sie den Startbetrag ein.");
        zinssatz = SimpleIO.getDouble("Bitte geben Sie den Zinssatz als Prozentwert ein.");
        System.out.println(startbetrag + " und" + zinssatz);
        System.out.println("Bitte waehlen Sie aus:");
        System.out.println("Ziel : Berechnet die Zeit, bis ein gegebener Betrag angespart wurde.");
        System.out.println("Zeit : Berechnet den Betrag, der nach einer gegebenen Zeit angespart wurde.");
        String wahl = SimpleIO.getString("Ihre Wahl:");
        switch (wahl) {
            case "Ziel" -> {
                double res = startbetrag;
                double zielbetrag = SimpleIO.getDouble("Bitte geben Sie den Zielbetrag ein.");
                int jahr = 0;
                while (res < zielbetrag) {
                    jahr += 1;
                    res = (1 + 0.01 * zinssatz) * res;
                }
                System.out.println("Es dauert " + jahr + " Jahre bei einem Zinssatz von " + zinssatz + "%, um von "
                        + startbetrag + " auf den Betrag " + zielbetrag + " zu sparen. Nach dieser Zeit hat man " + res
                        + ".");
            }

            case "Zeit" -> {
                double res = startbetrag;
                int jahr = SimpleIO.getInt("Bitte geben Sie die Anzahl der Jahre ein.");

                for (int i = 0; i < jahr; i++) {
                    res = (1 + 0.01 * zinssatz) * res;
                }
                System.out.println("Bei einem Zinssatz von " + zinssatz + "% und einem Startbetrag von " + startbetrag
                        + " hat man nach " + jahr + " Jahren " + res + " gespart.");
            }

            default -> System.out.println("Falsche Eingabe");
        }
    }

}